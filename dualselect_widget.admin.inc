<?php
/**
 * @file
 * dualselect_widget.admin.inc
 * Drupal API functions for the Module Admin Settings
 */
function dualselect_widget_admin_settings() {
  $form = array();

  $form['classes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Classes'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['classes']['dsw_class_operator'] = array(
    '#type' => 'radios',
    '#title' => t('Limit widget use to'),
    '#default_value' => variable_get('dsw_class_operator', 0),
    '#options' => array(
      t('Replace every select-multiple form field except those with these classes.'),
      t('Replace only select-multiple form fields with these classes.')
    ),
  );

  $form['classes']['dsw_class_values'] = array(
  '#type' => 'textarea',
  '#title'  => t('Classes'),
  '#description'  => t('Enter one class per line.'),
  '#default_value'  => variable_get('dsw_class_values', ''),
  );

  return system_settings_form($form);
}
