-- SUMMARY --

The Dualselect widget module defines a multiple selection interface
for select multiple form fields, to allow easier multi-selection for users.
It works in an unobtrusive fashion, by hiding the ugly and frustrating html
multiple select form field, and showing a sexier face to the world. There is
no extra markup needed and it doesn't change the drupal form api (FAPI).

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/jjemmett/1261152

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1261152


-- REQUIREMENTS --

This module depends on the jQuery_ui module (http://drupal.org/project/jquery_ui)
with the 1.6+ verson of the library.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  dualselect_widget module:

  - administer dualselect

    Users in roles with the "administer dualselect" permission will be able to
    access the Administer >> Site configuration >> Dualselect Widget Settings page.

* Customize the widget settings in Administer >> Site configuration >>
  Dualselect Widget Settings.
  
  To specify which select-multiple form fields get a makeover, follow the instructions
  for adding class limitations.

-- CUSTOMIZATION --

* To alter the style of the dualselect_widget interface:

  Override the dualselect.css file defaults by copying select lines from dualselect.css 
  to the style.css file in your theme.  Because the theme css is added after module css 
  any changes you make will override the default.


-- TROUBLESHOOTING --

* If the widget interface does not display, check the following:

  - Is there a javascript error being displayed in your browser's console?
  
  Dualselect does conflice with certain javascript code. When we find them we attempt to
  remove the conflict. One known conflict is with the IMCE module, and we have specificly
  removed dualselect from any page with IMCE running on it.

  - Is there a conflict with the specified class limitations?

  Because the limiting factors are html classes, there is always the possibility that
  removing the interface from one select list will unintentionally remove it from others.


-- CONTACT --

Current maintainers:
* John C. Jemmett (jjemmett) - http://drupal.org/user/21352
* Josh Clark (zanix) - http://drupal.org/user/1132616

This project has been sponsored by:
* North Wind Group
  A small business leader in the environmental, engineering, and construction service 
  industries. As well as having an I.T. staff that loves Drupal.
