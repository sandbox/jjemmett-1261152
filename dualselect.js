(function ($) {
if(jQuery.ui){
  Drupal.behaviors.dualselect = {
    attach: function(context){
      jQuery(Drupal.settings.dsw_selector).dualselect();
    }
  };

/*
 * Dual Select JQuery Widget
 *
 * Original Javascript code Copyright (c) 2009 Giancarlo Bellido
 * http://hackerhosting.com/ph-jquery
 *
 * Updates and revisions for Drupal by John Jemmett (jjemmett) and Josh Clark (zanix) 2011.
 *
 */
  jQuery.widget("ui.dualselect", {

    container: null,
    clear: null,

    lc: null,  // left container
    ll: null,  // left list
    lt: null,  // left toolbar
    cc: null,  // center container
    ct: null,  // center toolbar
    rc: null,  // right container
    rl: null,  // right list
    rt: null,  // right toolbar
    sel: null, // the actual select list
    fil: null, // optional filterer object

    _init_components: function() {
      this.lc = document.createElement("div");
      $(this.lc).addClass('lc');
      this.cc = document.createElement("div");
      $(this.cc).addClass('cc');
      this.rc = document.createElement("div");
      $(this.rc).addClass('rc');

      this.ll = document.createElement("ul");
      $(this.ll).addClass('ll');
      this.rl = document.createElement("ul");
      $(this.rl).addClass('rl');

      this.lt = document.createElement("div");
      $(this.lt).addClass('lt');
      this.ct = document.createElement("div");
      $(this.ct).addClass('ct');
      this.rt = document.createElement("div");
      $(this.rt).addClass('rt');
    },

    _init_size: function() {
      var _calculateWidth = function(obj) {
        var size = obj.width();
        return (size) ? ((size * 2) + 33) : 333;
      };
      var _calculateHeight = function(obj) {
        var size = obj.attr("size");
        var lineheight = parseInt(obj.css('line-height'));
        return (size) ? size * lineheight : 125;
      };
      $(this.container).css('width', _calculateWidth(this.element) + "px");
      this.rl.style.height = _calculateHeight(this.element) + "px";
      this.ll.style.height = this.rl.style.height;
    },

    _init_elements: function() {
      var self = this;

      this.sel.children().each(function(i) {
        if((this.innerHTML == '- None -' || this.innerHTML == '- Select One -') && !this.value){  // major hack to remove the - None - field from multiselect in drupal
        }else{
        var li = document.createElement("li");
        var element_value = this.value;
        $(li).attr('data',element_value); // can't use attribute 'value' because it has an issue with strings
        $(li).addClass(this.className);
        $(li).attr('index',i);
        $(li).click(function(){ $(this).toggleClass('selected');});
        $(li).html(this.innerHTML);
        $(li).css('font-size',$(self.sel).css('font-size'));
        $(li).css('line-height',(parseInt($(self.sel).css('line-height'))-1) +"px");

        if (this.selected) {
          $(li).dblclick(function(e) { self.move($(e.target).removeClass('selected'),self.ll) });
          $(self.rl).append(li);
        } else {
          $(li).dblclick(function(e) { self.move($(e.target).removeClass('selected'),self.rl) });
          $(self.ll).append(li);
        }
        }
      });
    },

    _init: function() {
      this.sel = this.element;

      this.options.sorted = true;
      this.container = $('<div id="dualselect_' + this.element.attr("id") + '" class="dual-select"></div>').insertAfter(this.element);
      this.clear = $('<div class="clearfix"></div>').insertAfter(this.container);
      this.container.data("dualselect", this.element.data("dualselect"));

      var self = this;

      this._init_components();
      this._init_elements();

      this.container.append(this.lc).append(this.cc).append(this.rc);

      this.lc.appendChild(this.ll);
      this.lc.appendChild(this.lt);

      this.cc.appendChild(this.ct);

      this.rc.appendChild(this.rl);
      this.rc.appendChild(this.rt);

      $(this.lt).html(
        "<div class='toggle-handle' title='" + Drupal.t("Search Add options") + "'><div></div></div>"+
        "<div>"+
        "<table id='lt-search-table'><tr><td class='dualselect-search-cell'>"+
        "<input type='text' class='dualselect-search' /></td><td class='clear-cell'>"+
        "<a href='#' title='" + Drupal.t("Clear Search") + "' class='clearsearch'><span class='icon circle-close'></span></a>"+
        "</td></tr></table></div>"
      ).addClass("toolbar");

      $(this.lt).find('.dualselect-search').unbind('keydown').unbind('keypress').unbind('keyup');
      $(this.lt).find('.dualselect-search').bind('keydown keypress', function(e) { if (e.which == 13) { e.preventDefault(); }});
      $(this.lt).find('.dualselect-search').bind('keyup', function(e) {
        e.preventDefault();
        if (!e.which && ((e.charCode || e.charCode === 0) ? e.charCode: e.keyCode)) {
           e.which = e.charCode || e.keyCode;
        }
        if (e.which == 13) {
          self.add();
        } else {
          self.search(this, self.ll)
        }
      });
      $(this.lt).find('.clearsearch').unbind().click(function() {
        self.clearSearch($(self.lt).find('.dualselect-search'), self.ll);
        this.blur();
        return false;
      });

      $(this.ct).html(
        '<div id="icons">'+
        "<a href='#' title='" + Drupal.t("Add All") + "' class='add-all'><span class='icon seek-next'></span></a>"+
        "<a href='#' title='" + Drupal.t("Add Selected") + "' class='add'><span class='icon arrowthick-1-e'></span></a>"+
        "<a href='#' title='" + Drupal.t("Remove Selected") + "' class='remove'><span class='icon arrowthick-1-w'></span></a>"+
        "<a href='#' title='" + Drupal.t("Remove All") + "' class='remove-all'><span class='icon seek-prev'></span></a>"+
        "</div>"
      );
      $(this.ct).find(".add-all").unbind().click(function() { self.add_all(); this.blur(); return false; });
      $(this.ct).find(".add").unbind().click(function() { self.add(); this.blur(); return false; });
      $(this.ct).find(".remove").unbind().click(function() { self.remove(); this.blur(); return false; });
      $(this.ct).find(".remove-all").unbind().click(function() { self.remove_all(); this.blur(); return false; });

      $(this.rt).html(
        "<div class='toggle-handle' title='" + Drupal.t("Search Remove options") + "'><div></div></div>"+
        "<div>"+
        "<table id='rt-search-table'><tr><td class='dualselect-search-cell'>"+
        "<input type='text' class='dualselect-search' /></td><td class='clear-cell'>"+
        "<a href='#' title='" + Drupal.t("Clear Search") + "' class='clearsearch'><span class='icon circle-close'></span></a>"+
        "</td></tr></table></div>"
      ).addClass("toolbar");

      $(this.rt).find('.dualselect-search').unbind('keydown').unbind('keypress').unbind('keyup');
      $(this.rt).find('.dualselect-search').bind('keydown keypress', function(e) { if (e.which == 13) { e.preventDefault(); }});
      $(this.rt).find('.dualselect-search').bind('keyup',function(e) {
        e.preventDefault();
        if (!e.which && ((e.charCode || e.charCode === 0) ? e.charCode: e.keyCode)) {
           e.which = e.charCode || e.keyCode;
        }
        if (e.which == 13) {
          self.remove();
        } else {
          self.search(this, self.rl)
        }
      });
      $(this.rt).find('.clearsearch').unbind().click(function() {
        self.clearSearch($(self.rt).find('.dualselect-search'), self.rl);
        this.blur();
        return false;
      });
      $('.toggle-handle').unbind().click(function() {
          $(this).next().slideToggle();
          $(this).find('div').toggleClass('expanded');
          $(this).next().find('input.dualselect-search').focus();
      }).next().css('display','none');//Using css display none here instead of hide due to a bug in jquery_ui 1.6

      this._init_size();

      this.sel.css('display','none');// jquery_ui 1.6 bug fix

    },

    /* METHODS */

    /**
     * Adds an element from the left to the right list.
     */
    add_all: function() {
      var sort = this.options.sorted;
      this.options.sorted = 0;
      this.move(this.notSelected(':visible').removeClass('selected'), this.rl);
      this.clearSearch($(this.lc).find('input.dualselect-search'),this.ll);
      this.options.sorted = sort;
    },
    add: function() {
      this.move(this.notSelected('.selected:visible').removeClass('selected'), this.rl);
      this.clearSearch($(this.lc).find('input.dualselect-search'),this.ll);
    },

    /**
     * Removes an element from the right to the left list.
     */
    remove: function() {
      this.move(this.selected(".selected:visible").removeClass('selected'), this.ll);
      this.clearSearch($(this.rc).find('input.dualselect-search'),this.rl);
    },
    remove_all: function() {
      var sort = this.options.sorted;
      this.options.sorted = 0;
      this.move(this.selected(":visible").removeClass('selected'), this.ll);
      this.clearSearch($(this.rc).find('input.dualselect-search'),this.rl);
      this.options.sorted = sort;
    },

    /** Moves elements to list */
    move: function(elements, list) {
      var self = this;
      // are we adding or removing?
      var type = ($(list).hasClass('ll')?'rem':'add');
      var list2 = ($(list).hasClass('ll')?self.rl:self.ll);

      if (this.options.sorted && elements.length < 300) { // elements length check for long lists that time out.
        var l2 = $(list).children('li[index]');

        elements.each(function() {
          var i = 0;
          var a = $(this).attr("index")*1;
          while (a > (l2.eq(i).attr("index")*1) && i < l2.length) {
            i = i + 1;
          }

          if (i == l2.length) {
            $(list).append(this);
          } else {
            l2.eq(i).before(this);
          }
        });
      } else {
        $(list).append(elements);
      }
      elements.each(function() {
        var a = $(this).attr("index")*1;
        if (type=='rem') {
          $(self.sel).find('option[value='+a+']').removeAttr('selected');
        } else {
          $(self.sel).find('option[value='+a+']').attr('selected','selected');
        }
        $(this).unbind('dblclick');
        $(this).dblclick(function(e) { self.move($(e.target).removeClass('selected'),list2) });
      });
    },

    /**
     * Returns Selected Elements as a jQuery Object.
     */
    selected: function(selector) {
      return $(this.rl).children(selector);
    },

    notSelected: function(selector) {
      return $(this.ll).children(selector);
    },

    /**
     * Filters list children Based on input's value.
     */
    search: function(input, list) {
      var regex = new RegExp(input.value, "i");

      $(list).children().removeClass('selected');
      $(list).children().show().each(function() {
        if (!regex.test($(this).text())) {
          $(this).hide();
        }
      });
      $(list).children(':visible:first').addClass('selected');
    },
    /**
     * Shows all the elements in the list previosly filters
     */
    resetSearch: function(list) {
      $(list).children().show();
      if (this.fil) { $(this.fil).val(''); }
    },

    showSearch: function(sb, list) {
      if ($(sb).is(':visible')) {
        this.closeSearch(sb, list);
      } else {
        $(sb).show();
        $(list).height($(list).height() - $(sb).outerHeight());
      }
    },

    clearSearch: function(sb, list) {
      $(sb).val("");
      this.resetSearch(list);
    },
    closeSearch: function(sb, list) {
      $(list).height($(list).height() + $(sb).outerHeight());
      $(sb).hide();
      this.resetSearch(list);
    },

    /* Returns Array containing values of selected items */
    values: function() {
      var v = [];
      this.selected().each(function() {
        v.push($(this).attr("data"));
      });

      return v;
    },

    /* Returns array containing the text of the selected items */
    selectedText: function() {
      var v = [];
      this.selected().each(function() { v.push($(this).text()); });
      return v;
    },

    /* Returns array containing the html of the selected items */
    selectedHtml: function() {
      var v = [];
      this.selected().each(function() { v.push($(this).html()); });
      return v;
    },
    registerFilterer: function(id) {
      this.fil = $("#"+id);
    }

  });

  jQuery.ui.dualselect.getter = [ "selected", "notSelected", "values", "selectedText", "selectedHtml" ];
}

